<?php

namespace App\Http\Controllers;

class PagesController extends Controller {

  public function getIndex() {
    return view('pages/welcome');
  }

  public function getAbout() {
    $fullname = 'Alex';
    $email = 'my@hgg.es';

    $data = [];
    $data['name'] = $fullname;
    $data['email'] = $email;

    return view('pages/about')->withData($data);
  }

  public function getContact() {
    return view('pages/contact');
  }
}
 ?>
