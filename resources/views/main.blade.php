@include('_head')
<body>
<!-- Nav -->
@include('_nav')
<!-- Nav End  -->
<!-- Header -->
<header class="header flex-center">
  <h2 class="postTitle">Welcome to my test blog site make with Laravel</h2>
  <button class="btn">Populare</button>
</header>
<!--Header End  -->
<!-- Content -->
<div class="content flex-center boxPading">
  @include('partials._messages')
  @yield('content')

</div>
<!-- ontent End -->
</body>
</html>
