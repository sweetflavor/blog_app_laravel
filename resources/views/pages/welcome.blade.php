@extends('main')

@section('content')
<div class="flex-center flex-respons">
  <div class="postCart boxPading">
    <header class="postHeader">
      <h4 class="postTitle">Test title</h4>
    </header>
    <section class="postContent">
        <article>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis laudantium ex aperiam voluptatum earum aliquam sed officiis maiores eos, temporibus rem ea blanditiis optio sunt amet nihil quas nam doloremque.</p>
        </article>
        <button class="btn readPost">Read More</button>
    </section>
  </div>

  <div class="postCart boxPading">
    <header class="postHeader">
      <h4 class="postTitle">Test title</h4>
    </header>
    <section class="postContent">
        <article>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis laudantium ex aperiam voluptatum earum aliquam sed officiis maiores eos, temporibus rem ea blanditiis optio sunt amet nihil quas nam doloremque.</p>
        </article>
        <button class="btn readPost">Read More</button>
    </section>
  </div>

  <div class="postCart boxPading">
    <header class="postHeader">
      <h4 class="postTitle">Test title</h4>
    </header>
    <section class="postContent">
        <article>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis laudantium ex aperiam voluptatum earum aliquam sed officiis maiores eos, temporibus rem ea blanditiis optio sunt amet nihil quas nam doloremque.</p>
        </article>
        <button class="btn readPost">Read More</button>
    </section>
  </div>
</div>
  @endsection
