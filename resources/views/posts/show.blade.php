@extends('main')

@section('content')
<div class="postPage">

  <h1>{{ $post->title }}</h1>
  <br>
  <dl class="data-box">
    <dt>Create At: {{ date('M j, Y', strtotime($post->created_at)) }}</dt>
  </dl>
  <p>{{ $post->body }}</p>
  <div class="postMenu">
    {!! Html::linkRoute('posts.edit', 'Edit', array($post->id), array('class' => 'btn')) !!}
    {!! Html::linkRoute('posts.destroy', 'Delete', array($post->id), array('class' => 'btn btn__denger')) !!}
  </div>
</div>
@endsection
