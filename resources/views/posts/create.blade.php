@extends('main')

@section('content')

  <h4 class="postTitle">Create a New Post</h4>
  {!! Form::open(array('route' => 'posts.store')) !!}
      {{ form::label('title', 'Title:') }}
      <br>
      {{ form::text('title', null, array('class' => 'form-control')) }}
      <br>
      {{ form::label('body', 'Post Body:') }}
      <br>
      {{ form::text('body', null, array('class' => 'form-control')) }}
      <br>
      {{ form::submit('Create Post', array('class' => 'btn')) }}
  {!! Form::close() !!}

@endsection
